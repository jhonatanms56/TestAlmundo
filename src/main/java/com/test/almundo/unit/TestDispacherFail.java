package com.test.almundo.unit;

import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;
import com.anarsoft.vmlens.concurrent.junit.ThreadCount;
import com.test.almundo.controller.Dispatcher;
import com.test.almundo.controller.EmpleadoController;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;


@RunWith(ConcurrentTestRunner.class)
public class TestDispacherFail {

    private Dispatcher disp = new Dispatcher();
    private EmpleadoController empcont = new EmpleadoController();

    @Rule
    public ExpectedException thrown = ExpectedException.none();


    //Se ejecutan 15 llamadas concurrentes se arroja una excepcion generica controlada por la Clase Dispatcher
    // La cual muestra el mensaje Maximo de concurrencia superado

    @Test
    @ThreadCount(15)
    public void testAsignacionMasDeDiezLlamadasConcurrentesPorDispacher() throws Exception {
        disp.dispatchCall("cliente");
    }

    @After
    public void afterAsignacionMasDeDiezLlamadas() throws Exception {
        thrown.expectMessage("Expected exception");
    }
}
