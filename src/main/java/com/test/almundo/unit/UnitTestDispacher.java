package com.test.almundo.unit;

import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;
import com.anarsoft.vmlens.concurrent.junit.ThreadCount;
import com.test.almundo.controller.Dispatcher;
import com.test.almundo.controller.EmpleadoController;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.TestCase.assertEquals;

@RunWith(ConcurrentTestRunner.class)
public class UnitTestDispacher {

    private Dispatcher disp = new Dispatcher();
    private EmpleadoController empcont = new EmpleadoController();


    @Before
    public void before(){
        //Inicializamos la lista de Empleados antes de ejecutar cualquier prueba ya que para todas debemos tener esta lista precargada
        EmpleadoController.inicializarListaEmpleados();

    }

    @Test
    public void testInicializoListaEmpleadosExitosa(){
        // Se inicializa la lista de empleados de la siguiente forma
        // 4 Operadores
        // 3 Supervisores
        // 1 Director
        // Para un total de 8 Empleados que es la finalidad de este test verificar que los 8 empleados se han creado exitosamente
        assertEquals(8,EmpleadoController.listEmpleados.size());

    }


    @Test
    @ThreadCount(10)
    public void testAsignacionDeDiezLlamadasPorDispacher() throws Exception {
        //Se crean diez llamadas concurrentes al metodo dispatchCall
        disp.dispatchCall("cliente");

    }

    @After
    public void afterAsignacionDeDiezLlamadas() throws Exception {
        // Se verifica que se hallan asignado las 10 llamadas concurrentes de manera exitosa
        // La variable esta configurada para atender un maximo de 10 llamadas concurrentes
        // Tras cada llamada de los clientes se descuenta uno
        // Dejando un total de 0 luego de procesar las 10 llamadas concurrentes en el paso anterior

        assertEquals(0,EmpleadoController.maxLlamadasConcurrentes);

        //Se puede verificar la jerarquia establecida para las llamadas y la modificacion de disponiblidad del empleado
        // Se asignaron los 4 Operadores disponibles inicialmente
        // Luego se asignan los 3 Supervisores disponibles
        // Luego se asigna el Director esto segun la inicializacion que se dio de empleados en el metodo before
        // Luego de asignar todos estos empleados disponibles se muestra el mensaje :
        // "Estimado cliente Lo sentimos en el momento no se encuentran Empleados Disponibles para atender su llamada intente de nuevo mas tarde"
        // Dando un total de 8 Empleados asiganados a 8 de las 10 llamadas y a los otros dos se les muestra el mensaje anterior
        // Soportando la concurrencia maxima de 10 clientes aun cuando tenemos solo 8 empleados registrados en el proyecto
    }


}
