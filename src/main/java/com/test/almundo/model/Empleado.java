package com.test.almundo.model;

public class Empleado {
    private String nombre;
    private Boolean available;

    public Empleado(String nombre) {
        this.nombre = nombre;
        this.available = true;
    }

    public Empleado() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }
}
