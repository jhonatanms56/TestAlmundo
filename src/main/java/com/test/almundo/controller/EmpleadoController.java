package com.test.almundo.controller;

import com.test.almundo.model.Director;
import com.test.almundo.model.Empleado;
import com.test.almundo.model.Operador;
import com.test.almundo.model.Supervisor;

import java.util.ArrayList;

public class EmpleadoController {

    public static ArrayList<Empleado> listEmpleados = new ArrayList<Empleado>();

    public static int maxLlamadasConcurrentes = 10;

    public static void inicializarListaEmpleados(){
        createListEmployees();
    }

    private static void createListEmployees(){
        listEmpleados.add(new Operador("O1"));
        listEmpleados.add(new Operador("O2"));
        listEmpleados.add(new Operador("O3"));
        listEmpleados.add(new Operador("O4"));
        listEmpleados.add(new Supervisor("S1"));
        listEmpleados.add(new Supervisor("S2"));
        listEmpleados.add(new Supervisor("S3"));
        listEmpleados.add(new Director("D1"));
    }

    public int getMaxLlamadasConcurrentes() {
        return maxLlamadasConcurrentes;
    }

    public void descontarLlamadasConcurrentes(){
        maxLlamadasConcurrentes--;
    }
}
