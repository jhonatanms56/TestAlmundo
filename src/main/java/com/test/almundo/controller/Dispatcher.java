package com.test.almundo.controller;


public class Dispatcher implements Runnable {

    EmpleadoController empcont = new EmpleadoController();

    public Dispatcher(){
    }

    public void run(){
        String currentClient = Thread.currentThread().getName();
        try {
            dispatchCall(currentClient);
        } catch (Exception ex){
            System.out.println("Exception ejecutando Hilo " + ex.getMessage());
        }
    }

    //Metodo encargado de procesar las llamadas de los Clientes realiza asignacion segun disponibilidad
    public String dispatchCall(String currentClient) throws Exception {
        String resultMessage = "";
        if(totalClientesConcurrentes()) {
            resultMessage = "Estimado " + currentClient + " Su llamada fue Asignada a un : ";

            if (validateAvailableEmployee("Operador"))
                resultMessage += "Operador";
            else if (validateAvailableEmployee("Supervisor"))
                resultMessage += "Supervisor";
            else if (validateAvailableEmployee("Director"))
                resultMessage += "Director";
            else
                resultMessage = "Estimado " + currentClient + " Lo sentimos en el momento no se encuentran Empleados Disponibles para atender su llamada intente de nuevo mas tarde";

            System.out.println(resultMessage);
        }else{
            System.out.println("Maximo de concurrencia superada no se proceso el Cliente : " + Thread.currentThread().getName());
            throw new Exception("Maximo de concurrencia superado");
        }
        return resultMessage;
    }

    //Valida la disponibilidad de los Empleados y realiza la asignacion del mismo
    private synchronized Boolean validateAvailableEmployee(String tipoEmpleadoJerarquia) {

        for(int i=0; i<= EmpleadoController.listEmpleados.size()-1; i++){
            if(EmpleadoController.listEmpleados.get(i).getClass().getSimpleName().equalsIgnoreCase(tipoEmpleadoJerarquia) && EmpleadoController.listEmpleados.get(i).getAvailable()){
                EmpleadoController.listEmpleados.get(i).setAvailable(false);
                return true;
            }
        }
        return false;
    }

    //Descuenta y Verifica el maximo de llamadas concurrentes soportado para este caso 10
    private synchronized Boolean totalClientesConcurrentes(){
        Boolean resultado = false;
        if (empcont.getMaxLlamadasConcurrentes() > 0){
            empcont.descontarLlamadasConcurrentes();
            resultado = true;
        }
        else {
            Thread.currentThread().interrupt();
        }
        return  resultado;
    }

}
