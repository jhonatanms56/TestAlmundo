package com.test.almundo;

import com.test.almundo.controller.Dispatcher;
import com.test.almundo.controller.EmpleadoController;

public class Ejecutar {
    public static void main(String[] args){

        EmpleadoController.inicializarListaEmpleados();
        Dispatcher disp = new Dispatcher();

        Thread cliente1 = new Thread(disp,"cliente1");
        Thread cliente2 = new Thread(disp,"cliente2");
        Thread cliente3 = new Thread(disp,"cliente3");
        Thread cliente4 = new Thread(disp,"cliente4");
        Thread cliente5 = new Thread(disp,"cliente5");
        Thread cliente6 = new Thread(disp,"cliente6");
        Thread cliente7 = new Thread(disp,"cliente7");
        Thread cliente8 = new Thread(disp,"cliente8");
        Thread cliente9 = new Thread(disp,"cliente9");
        Thread cliente10 = new Thread(disp,"cliente10");
        Thread cliente11 = new Thread(disp,"cliente11");
        Thread cliente12 = new Thread(disp,"cliente12");



        cliente1.start();
        cliente2.start();
        cliente3.start();
        cliente4.start();
        cliente5.start();
        cliente6.start();
        cliente7.start();
        cliente8.start();
        cliente9.start();
        cliente10.start();
        cliente11.start();
        cliente12.start();


    }

}
